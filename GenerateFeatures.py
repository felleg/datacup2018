import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

"""
This script is meant to create the input files for training and testing.
Change it only if you want to change the features.
Run it once every time you change it.
"""


class GenerateFeatures:
    """
    This function loops over each account ID listed in df_performance (they are unique). For each ID, it will
    extract every feature and store them in separate lists. When we finished looping over each ID, these
    arrays are stored into a pandas dataframe called "features".

    Arguments:
        data: str (default='train')
            Use 'train' or 'test' to determine which type of datasets to use for analysis.
            If neither 'train' or 'test' is used, the program will stop.
    """
    def __init__(self,  data='train', *args, **kwargs):
        """
        This function loads the dataframes (train or test only) and launches GenerateFeatures()

        Arguments:
            data: str (default='train')
                Use 'train' or 'test' to determine which type of datasets to use for analysis.
                If neither 'train' or 'test' is used, the program will stop.
        """
        if data == 'train' or data == 'test':
            self.df_paiements = pd.read_csv('dataset/paiements_%s.csv'%(data)).replace(np.nan, 0, regex=True)
            print("Generated df_paiements.")
            self.df_facturation = pd.read_csv('dataset/facturation_%s.csv'%(data)).replace(np.nan, 0, regex=True)
            print("Generated df_facturation.")
            self.df_performance = pd.read_csv('dataset/performance_%s.csv'%(data)).replace(np.nan, 0, regex=True)
            print("Generated df_performance.")

            # Special case for transactions, take additional data into account
            transaction_files=['dataset/transactions_%s.csv'%(data),
                    'dataset/additional_transactions_%s.csv'%(data)]
            self.df_transactions = pd.concat([pd.read_csv(f).replace(np.nan, 0, regex=True) for f in transaction_files], ignore_index=True)
            print("Generated df_transactions with additional data provided by Desjardins (July 14 2018).")

            self.features = self.generate_features()
        else:
            print("Error, data must be 'train' or 'test'. Your input was data='%s'."%(data))

    def generate_features(self, *args, **kwargs):
        """
        This function loops over each account ID listed in df_performance (they are unique). For each ID, it
        will extract every feature and store them in separate lists. When we finished looping over each ID,
        these arrays are stored into a pandas dataframe called "features".

        Return:
            features: pandas.dataframe
                A pandas dataframe where each row is a unique account ID and each column contains a feature
                for this specific ID.
        """
        # Initialize arrays
        freq_payments = []
        freq_transactions = []
        n_delqcycle = []
        n_transactions = []
        n_payments = []

        n_paymentReveral_Qflags = []
        n_paymentReveral_Nflags = []

        mean_paymentsPeGetNPaymentsrMonth = []
        mean_transactionsPerMonth = []
        mean_n_transactionsPerMonth = []
        mean_n_paymentsPerMonth = []
        mean_currentTotalBalance = []
        mean_cashBalance = []
        mean_currentTotalBalance_creditLimit_ratio = []
        mean_cashBalance_creditLimit_ratio = []

        max_payment = []
        max_transaction = []
        max_statement = []
        max_creditLimit = []
        max_currentTotalBalance = []
        max_cashBalance = []
        max_currentTotalBalance_creditLimit_ratio = []
        max_cashBalance_creditLimit_ratio = []

        min_payment = []
        min_transaction = []
        min_statement = []
        min_creditLimit = []
        min_currentTotalBalance = []
        min_cashBalance = []
        min_currentTotalBalance_creditLimit_ratio = []
        min_cashBalance_creditLimit_ratio = []

        slope_cashBalance = []
        intercept_cashBalance = []

        account_IDs = self.df_performance.ID_CPTE.unique()
        for acc in account_IDs:
            # Print progress while the analysis is running
            if list(account_IDs).index(acc)%100 == 0:
                print('%s / %s'%(list(account_IDs).index(acc),len(account_IDs)))

            #freq_payments += [self.GetPaymentFreq(acc)]
            #freq_transactions += [self.GetTransactionFreq(acc)]
            n_delqcycle += [self.GetNDelqcycle(acc)]
            n_transactions += [self.GetNTransactions(acc)]
            n_payments += [self.GetNPayments(acc)]

            n_Qflags, n_Nflags = self.GetNPaymentReversalFlags(acc)
            n_paymentReveral_Qflags += [n_Qflags]
            n_paymentReveral_Nflags += [n_Nflags]

            #mean_paymentsPerMonth += [self.GetMeanPaymentsPerMonth(acc)]
            #mean_transactionsPerMonth += [self.GetMeanTransactionsPerMonth(acc)]
            #mean_n_transactionsPerMonth += [self.GetMeanNTransactionsPerMonth(acc)]
            #mean_n_paymentsPerMonth += [self.GetMeanNPaymentsPerMonth(acc)]
            mean_currentTotalBalance += [self.GetMeanCurrentTotalBalance(acc)]
            mean_cashBalance += [self.GetMeanCashBalance(acc)]
            mean_currentTotalBalance_creditLimit_ratio += [self.GetMean_CurrentTotalBalance_CreditLimit_Ratio(acc)]
            mean_cashBalance_creditLimit_ratio += [self.GetMean_CashBalance_CreditLimit_Ratio(acc)]

            max_payment += [self.GetMaxPayment(acc)]
            max_transaction += [self.GetMaxTransaction(acc)]
            max_statement += [self.GetMaxStatement(acc)]
            max_creditLimit += [self.GetMaxCreditLimit(acc)]
            max_currentTotalBalance += [self.GetMaxCurrentTotalBalance(acc)]
            max_cashBalance += [self.GetMaxCashBalance(acc)]
            max_currentTotalBalance_creditLimit_ratio += [self.GetMax_CurrentTotalBalance_CreditLimit_Ratio(acc)]
            max_cashBalance_creditLimit_ratio += [self.GetMax_CashBalance_CreditLimit_Ratio(acc)]

            min_payment += [self.GetMinPayment(acc)]
            min_transaction += [self.GetMinTransaction(acc)]
            min_statement += [self.GetMinStatement(acc)]
            min_creditLimit += [self.GetMinCreditLimit(acc)]
            min_currentTotalBalance += [self.GetMinCurrentTotalBalance(acc)]
            min_cashBalance += [self.GetMinCashBalance(acc)]
            min_currentTotalBalance_creditLimit_ratio += [self.GetMin_CurrentTotalBalance_CreditLimit_Ratio(acc)]
            min_cashBalance_creditLimit_ratio += [self.GetMin_CashBalance_CreditLimit_Ratio(acc)]

            slope, intercept = self.LinFitCashBalance(acc)
            slope_cashBalance += [slope]
            intercept_cashBalance += [intercept]

        features = pd.DataFrame({
            'ID_CPTE':account_IDs,
            #'freq_payments':freq_payments,
            #'freq_transactions':freq_transactions,

            'n_delqcycle':n_delqcycle,
            'n_transactions':n_transactions,
            'n_payments':n_payments,
            'n_paymentReveral_Qflags':n_paymentReveral_Qflags,
            'n_paymentReveral_Nflags':n_paymentReveral_Nflags,

            #'mean_paymentsPerMonth':mean_paymentsPerMonth,
            #'mean_transactionsPerMonth':mean_transactionsPerMonth,
            #'mean_n_transactionsPerMonth':mean_n_transactionsPerMonth,
            #'mean_n_paymentsPerMonth':mean_n_paymentsPerMonth,
            'mean_currentTotalBalance':mean_currentTotalBalance,
            'mean_cashBalance':mean_cashBalance,
            'mean_currentTotalBalance_creditLimit_ratio':mean_currentTotalBalance_creditLimit_ratio,
            'mean_cashBalance_creditLimit_ratio':mean_cashBalance_creditLimit_ratio,

            'max_payment':max_payment,
            'max_transaction':max_transaction,
            'max_statement':max_statement,
            'max_creditLimit':max_creditLimit,
            'max_currentTotalBalance':max_currentTotalBalance,
            'max_cashBalance':max_cashBalance,
            'max_currentTotalBalance_creditLimit_ratio':max_currentTotalBalance_creditLimit_ratio,
            'max_cashBalance_creditLimit_ratio':max_cashBalance_creditLimit_ratio,

            'min_payment':min_payment,
            'min_transaction':min_transaction,
            'min_statement':min_statement,
            'min_creditLimit':min_creditLimit,
            'min_currentTotalBalance':min_currentTotalBalance,
            'min_cashBalance':min_cashBalance,
            'min_currentTotalBalance_creditLimit_ratio':min_currentTotalBalance_creditLimit_ratio,
            'min_cashBalance_creditLimit_ratio':min_cashBalance_creditLimit_ratio,

            'slope_cashBalance':slope_cashBalance,
            'intercept_cashBalance':intercept_cashBalance,
        })
        return features

    def GetPaymentFreq(self, acc):
        pass
    def GetTransactionFreq(self, acc):
        pass
    def GetNDelqcycle(self, acc):
        # TODO(Felix): Verify if this function means anything. I return the sum because DelqCycle can contain values
        # between 0 and 5 (non-binary).
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return np.sum (df.DelqCycle)
        except:
            return 0

    def GetNTransactions(self, acc):
        df = self.df_transactions[self.df_transactions.ID_CPTE == acc]
        try:
            return len(df)
        except:
            return 0

    def GetNPayments(self, acc):
        df = self.df_paiements[self.df_paiements.ID_CPTE == acc]
        try:
            return len(df)
        except:
            return 0

    def GetNPaymentReversalFlags(self, acc):
        """NaN are not counted"""
        df = self.df_paiements[self.df_paiements.ID_CPTE == acc]
        try:
            q = len(df.PAYMENT_REVERSAL_XFLG[df.PAYMENT_REVERSAL_XFLG == "Q"])
            n = len(df.PAYMENT_REVERSAL_XFLG[df.PAYMENT_REVERSAL_XFLG == "N"])
            return q, n
        except:
            return 0, 0

    def GetMeanPaymentsPerMonth(self, acc):
        pass
    def GetMeanTransactionsPerMonth(self, acc):
        pass
    def GetMeanNTransactionsPerMonth(self, acc):
        pass
    def GetMeanNPaymentsPerMonth(self, acc):
        df = self.df_paiements[self.df_paiements.ID_CPTE == acc]
        #TODO(Felix) : select all data for the same month, calculate the mean over that, put in list, and finally
        #return the list

        # Here is what I started doing
        #dates = np.unique(df.TRANSACTION_DTTM)
        #for d in dates:
        #    tmp_df = df[df.TRANSACTION_DTTM.str.contains("dates")]

    def GetMeanCurrentTotalBalance(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return np.mean(df.CurrentTotalBalance)
        except:
            return 0

    def GetMeanCashBalance(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return np.mean(df.CashBalance)
        except:
            return 0

    def GetMean_CurrentTotalBalance_CreditLimit_Ratio(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        currentTotalBalance = np.array(df.CurrentTotalBalance)
        creditLimit = np.array(df.CreditLimit)
        try:
            return np.mean(currentTotalBalance / creditLimit)
        except:
            return 0

    def GetMean_CashBalance_CreditLimit_Ratio(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        cashBalance = np.array(df.CashBalance)
        creditLimit = np.array(df.CreditLimit)
        try:
            return np.mean(cashBalance / creditLimit)
        except:
            return 0

    def GetMaxPayment(self, acc):
        df = self.df_paiements[self.df_paiements.ID_CPTE == acc]
        try:
            return max(df.TRANSACTION_AMT)
        except:
            return 0

    def GetMaxTransaction(self, acc):
        df = self.df_transactions[self.df_transactions.ID_CPTE == acc]
        try:
            return max(df.TRANSACTION_AMT)
        except:
            return 0

    def GetMaxStatement(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return max(df.CurrentTotalBalance)
        except:
            return 0

    def GetMaxCreditLimit(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return max(df.CreditLimit)
        except:
            return 0

    def GetMaxCurrentTotalBalance(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return max(df.CurrentTotalBalance)
        except:
            return 0

    def GetMaxCashBalance(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return max(df.CashBalance)
        except:
            return 0

    def GetMax_CurrentTotalBalance_CreditLimit_Ratio(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        currentTotalBalance = np.array(df.CurrentTotalBalance)
        creditLimit = np.array(df.CreditLimit)
        try:
            return max(currentTotalBalance / creditLimit)
        except:
            return 0

    def GetMax_CashBalance_CreditLimit_Ratio(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        cashBalance = np.array(df.CashBalance)
        creditLimit = np.array(df.CreditLimit)
        try:
            return max(cashBalance / creditLimit)
        except:
            return 0

    def GetMinPayment(self, acc):
        df = self.df_paiements[self.df_paiements.ID_CPTE == acc]
        try:
            return min(df.TRANSACTION_AMT)
        except:
            return 0

    def GetMinTransaction(self, acc):
        df = self.df_transactions[self.df_transactions.ID_CPTE == acc]
        try:
            return min(df.TRANSACTION_AMT)
        except:
            return 0

    def GetMinStatement(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return min(df.CurrentTotalBalance)
        except:
            return 0

    def GetMinCreditLimit(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return min(df.CreditLimit)
        except:
            return 0

    def GetMinCurrentTotalBalance(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return min(df.CurrentTotalBalance)
        except:
            return 0

    def GetMinCashBalance(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        try:
            return min(df.CashBalance)
        except:
            return 0

    def GetMin_CurrentTotalBalance_CreditLimit_Ratio(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        currentTotalBalance = np.array(df.CurrentTotalBalance)
        creditLimit = np.array(df.CreditLimit)
        try:
            return min(currentTotalBalance / creditLimit)
        except:
            return 0

    def GetMin_CashBalance_CreditLimit_Ratio(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        cashBalance = np.array(df.CashBalance)
        creditLimit = np.array(df.CreditLimit)
        try:
            return min(cashBalance / creditLimit)
        except:
            return 0

    def LinFitCashBalance(self, acc):
        df = self.df_facturation[self.df_facturation.ID_CPTE == acc]
        #convert the date column from string to datetime format
        df = df.astype({"PERIODID_MY":"datetime64[ns]"})
        #transform raw date into time differential since the first date
        df.PERIODID_MY = df.PERIODID_MY - df.PERIODID_MY.min()
        #convert into a float of number of days
        df = df.astype({"PERIODID_MY": "timedelta64[D]"})
        #do linear fit; no need to sort the data in chronological order
        slp, inter = np.polyfit(df.PERIODID_MY, df.CurrentTotalBalance, deg=1)
        return slp, inter






if __name__ == "__main__":
    datatype = ["train", "test"]
    for type in datatype:
        try:
            print("Generating for type:", type)
            c = GenerateFeatures(data=type)
            filename = "dataset/features_" + type + ".csv"
            c.features.replace(np.nan, 0, regex=True, inplace=True)
            c.features.to_csv(filename, index=False) #index=False is necessary, otherwise it will be loaded as a feature
            print("Created", filename)
        except Exception as e:
            print("There was an error: ", e)

