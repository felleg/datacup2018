
"""
@author: mp, Benoît Boucher
"""
import pandas as pd
import numpy as np
from sklearn.externals import joblib
from sklearn.ensemble import VotingClassifier, RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier



###============================Load Data===================================================

df_features = pd.read_csv('dataset/features_train.csv')
df_target = pd.read_csv("dataset/performance_train.csv")
if df_features.isna().any().any():
    print("Warning: detected NaN values")

X = df_features.values
# remove the first column, which is really the index
X = X[:, 1:]
y = df_target.loc[:,"Default"].values

X_train, X_test, y_train, y_test = train_test_split(X, y)

###============================Ensemble classifier===================================================

# create different classifiers
estimators = []
model1 = LogisticRegression(C=50.0, tol=0.01, solver='sag', max_iter=300)
estimators.append(('Logistic', model1))
model2 = RandomForestClassifier(n_estimators=150, max_depth=7, max_features="log2", min_samples_split=15)
estimators.append(('RandomForest', model2))
model3 = KNeighborsClassifier(n_neighbors=10, leaf_size=3, algorithm="ball_tree")
estimators.append(('KnearestN', model3))


#model
clf = VotingClassifier(estimators, voting="soft")
clf.fit(X_train, y_train)

#test the model
train_score = clf.score(X_train, y_train)
test_score = clf.score(X_test, y_test)


#save model to disk for later use by GenerateSolution.py
filepath = "classifiers/WIP.pkl"
joblib.dump(clf, filepath)
print("\nWrote: " + filepath)


print("\n" + "-"*40 + "\n")
print("Train score:", train_score)
print("Test score: ", test_score)
y_predict = clf.predict(X_test)
unique, counts = np.unique(y_predict, return_counts=True)
print("\n{Labels: count}\n", dict(zip(unique, counts)))

# #Results
# Train score: 0.873949579832
# Test score:  0.84268907563
