
"""
Created on Fri Jul 13 21:00:47 2018

@author: mp
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from pandas import read_csv
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_score
from sklearn.datasets import make_blobs 
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn import svm
from sklearn.svm import NuSVC
from sklearn.svm import LinearSVC
from sklearn.externals import joblib

##============================Load Data===================================================

df_features = pd.read_csv('dataset/features_train.csv').fillna(0)
df_features.head()
df_target = pd.read_csv("dataset/performance_train.csv")
df_target.head()
X = df_features.values
X=X[:,1:28]
y = df_target.loc[:,"Default"].values
X_train, X_test, y_train, y_test = train_test_split(X, y)

##============================Extremely Randomized Trees=====================================

clf_ERT = ExtraTreesClassifier(n_estimators=129, max_depth=None, min_samples_split=4, random_state=0)
###########################################################################################

clf_ERT.fit(X_train, y_train)
predicted_ERT = clf_ERT.predict(X_test)

##============================Scores=======================================================

train_score_ERT = clf_ERT.score(X_train, y_train)
test_score_ERT = clf_ERT.score(X_test, y_test)
print("Train score RandomForest :", train_score_ERT)
print("Test score RandomForest:", test_score_ERT)

############################################################################################
##the obtained results: 
#Train score RandomForest : 1.0
#Test score RandomForest: 0.8621848739495799

#save model to disk for later use by GenerateSolution.py
filepath = "classifiers/ERT.pkl"
joblib.dump(clf_ERT, filepath)
print("\nWrote: " + filepath)