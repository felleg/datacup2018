import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from keras import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.callbacks import ModelCheckpoint
from sklearn.decomposition import PCA
"""
This script is meant to be changed so you can try various Keras models.
Once satisfied, give a meaningful name to your model and
 run GenerateSolution.py
 Please exclude your WIP.h5 from your git push so that we don't download
 it by mistake and create confusion afterwards.
"""

#load and preprocess data
df_features = pd.read_csv('dataset/features_train.csv')
df_target = pd.read_csv("dataset/performance_train.csv")
if df_features.isna().any().any():
    print("Warning: detected NaN values")

X = df_features.values
#remove the first column, which is really the index
X = X[:,1:]
y = df_target.loc[:,"Default"].values


# feature selection
n_comp = 10
pca = PCA(n_components=n_comp)
fit = pca.fit(X)
X = pca.transform(X)


X_train, X_test, y_train, y_test = train_test_split(X, y)


#####PUT YOUR OWN KERAS MODEL HERE ##############

model = Sequential()
model.add(Dense(units=3, activation="relu", input_dim=n_comp))
model.add(Dense(units=5, activation="relu"))
# model.add(Dropout(0.2))
# model.add(Dense(units=120, activation="relu"))
# model.add(Dropout(0.2))
# model.add(Dense(units=12, activation="relu"))
# model.add(Dropout(0.1))
# model.add(Dense(units=70, activation="relu"))
# model.add(Dropout(0.1))
model.add(Dense(units=1, activation="sigmoid"))

#Compile the model using a loss function and an optimizer.
model.compile(loss="binary_crossentropy",
              optimizer="adam",
              metrics=["accuracy"])

#################################################

#fit and test the model
f_path = "temp/weights.hdf5"
checkpointer = ModelCheckpoint(filepath=f_path, verbose=1, save_best_only=True)
model.fit(X_train, y_train, batch_size=1, epochs=5,
          validation_data=(X_test, y_test), callbacks=[checkpointer])

_, train_score = model.evaluate(X_train, y_train, verbose=0)
_, test_score = model.evaluate(X_test, y_test, verbose=0)

print("\n" + "-"*40 + "\n")
print("Train score:", train_score)
print("Test score: ", test_score)
print("Dumb score: ~0.804033613485")

y_predict = model.predict_classes(X_test)
unique, counts = np.unique(y_predict, return_counts=True)
print("{Labels: count}\n", dict(zip(unique, counts)))
print("\n" + "-"*40 + "\n")

#save model to disk for later use by GenerateSolution.py
f_path = "classifiers/WIP.h5"
try:
    model.save(f_path)
    print("Wrote: " + f_path)
except:
    print("Couldn't save model to disk")