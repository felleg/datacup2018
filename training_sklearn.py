import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier



"""
This script is meant to be changed so you can try various sklearn models.
Once satisfied, give a meaningful name to your model and
 run GenerateSolution.py
 Please exclude your WIP.pkl from your git push so that we don't download
 it by mistake and create confusion afterwards.
"""

#load and preprocess data
df_features = pd.read_csv('dataset/features_train.csv')
df_target = pd.read_csv("dataset/performance_train.csv")
if df_features.isna().any().any():
    print("Warning: detected NaN values")

X = df_features.values
# remove the first column, which is really the index
X = X[:, 1:]
y = df_target.loc[:,"Default"].values

# #feature selection... it lowers accuracy most of the time
# n_comp = 10
# pca = PCA(n_components=n_comp)
# pca.fit(X)
# X = pca.transform(X)

X_train, X_test, y_train, y_test = train_test_split(X, y)


#####PUT YOUR OWN SKLEARN MODEL HERE ####################

#Train score: 0.83137254902 (no PCA)
#Test score:  0.831932773109 (no PCA)
#Train score: 0.622745098039 (with PCA)
#Test score:  0.610084033613 (with PCA)
#clf = LogisticRegression(C=50.0, tol=0.01, solver='sag', max_iter=300)

# # Train score: 0.988907563025 (no PCA)
# # Test score:  0.841008403361 (no PCA)
# # Train score: 0.988235294118 (with PCA
# # Test score:  0.819831932773 (with PCA)
# clf = RandomForestClassifier()

# #Train score: 0.887282913165 (no PCA)
# #Test score:  0.866554621849 (no PCA)
# #Train score: 0.86756302521 (with PCA)
# #Test score:  0.821848739496 (with PCA)
# clf = RandomForestClassifier(n_estimators=150, max_depth=7, max_features="log2", min_samples_split=15)

# #bad, all same label (no PCA)
# #bad, all same label (with PCA)
# clf = SVC()

# #bad, all same label (no PCA)
# #bad, all same label (with PCA)
# clf = SVC(C=1000, kernel="rbf", degree=5, tol=1e-4)

#Train score: 0.852100840336 (no PCA)
#Test score:  0.833949579832 (no PCA)
#Train score: 0.83081232493 (with PCA)
#Test score:  0.822521008403 (with PCA)
clf = KNeighborsClassifier(n_neighbors=10, leaf_size=3, algorithm="ball_tree")

#################################################


clf.fit(X_train, y_train)

#test the model
train_score = clf.score(X_train, y_train)
test_score = clf.score(X_test, y_test)

#save model to disk for later use by GenerateSolution.py
filepath = "classifiers/WIP.pkl"
joblib.dump(clf, filepath)
print("\nWrote: " + filepath)

print("\n" + "-"*40 + "\n")
print("Train score:", train_score)
print("Test score: ", test_score)
y_predict = clf.predict(X_test)
unique, counts = np.unique(y_predict, return_counts=True)
print("\n{Labels: count}\n", dict(zip(unique, counts)))