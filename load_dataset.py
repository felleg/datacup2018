import pandas as pd
import numpy as np

#Each ID_CPTE is listed in the same order as in df_performance; however,
# they are not unique and appear a variable number of times, which is to
# be expected since this file lists all payments made by each account
# Note that the transactions are not in chronological order
df_paiements_train = pd.read_csv('dataset/paiements_train.csv')
df_paiements_test = pd.read_csv('dataset/paiements_test.csv')

#Each ID_CPTE have 14 entries; they follow 14 consecutive months in
# PERIODID_MY. However, those months are not in chronological order.
# Also, not every account covers the same period
df_facturation_train = pd.read_csv('dataset/facturation_train.csv')
df_facturation_test = pd.read_csv('dataset/facturation_test.csv')

#Same comment as df_paiments:
#Each ID_CPTE is listed in the same order as in df_performance; however,
# they are not unique and appear a variable number of times, which is to
# be expected since this file lists all payments made by each account
# Note that the transactions are not in chronological order
df_transactions_train = pd.read_csv('dataset/transactions_train.csv')
df_transactions_test = pd.read_csv('dataset/transactions_test.csv')

#This is our goal: predict if the account will default or not
#There are 11900 ID_CPTE. They are all unique, regardless of PERIODID_MY
#The period is always December 1st, but the year changes.
#That period always corresponds to the latest period in df_facturation
data = 'train'
# Special case for transactions, take additional data into account
transaction_files=['dataset/transactions_%s.csv'%(data),
        'dataset/additional_transactions_%s.csv'%(data)]
df_transactions_train = pd.concat([pd.read_csv(f).replace(np.nan, 0, regex=True) for f in transaction_files], ignore_index=True)

data = 'test'
# Special case for transactions, take additional data into account
transaction_files=['dataset/transactions_%s.csv'%(data),
        'dataset/additional_transactions_%s.csv'%(data)]
df_transactions_test = pd.concat([pd.read_csv(f).replace(np.nan, 0, regex=True) for f in transaction_files], ignore_index=True)

#if present, also load our engineered features
try:
    df_features_train = pd.read_csv('dataset/features_train.csv')
    df_features_test = pd.read_csv('dataset/features_test.csv')
except:
    print("Could not load 'features_*.csv'")
