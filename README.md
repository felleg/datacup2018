# Welcome to RawDataVector's repo for the Desjardins DataCup18
The main files really are training_sklearn.py and training_keras.py.
Use these to build your model.
Once satisfied, transform your model into a submittable solution file with
GenerateSolution.py

Note that the base dataset provided does not contain any features.
We engineered the features using GenerateFeatures.py.
If you think of more features to create, then that's where you should do it.
Don't forget to run it after you've modified it so that our models keep using
the most up to date feature list! 

The other .py files are models tried independantly.

The classifiers and solutions folder contained our saved results.

# Using branches with this git project
Push code in branches as much as possible instead of in the master.
Request for merge when your branch has progressed enough.

Access branches here: https://gitlab.com/felleg/datacup2018/branches

Please do not upload any WIP.* files. Rename them into something meaningful if you think they are worth keeping.


# Communication
The slack chat room (private):
https://datacup18.slack.com/messages/GBKPVGPPU

# Contest source
The Desjardins Datacup18 was a contest organized by Desjardins. The website was located [here](https://defidonnees.ca/)

## Missing Data
As per Desjardins' rules, we have removed the source data from the repo.

## Rank
Our model was good enough to appear on the leader board (rank 49).
The total number of participating teams was not disclosed, but my personal estimate is between 100-150. 
