
"""
Created on Fri Jul 13 21:00:47 2018

@author: mp
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from pandas import read_csv
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import GaussianNB

##============================Load Data===================================================

df_features = pd.read_csv('dataset/features_train.csv').fillna(0)
df_features.head()
df_target = pd.read_csv("dataset/performance_train.csv")
df_target.head()
X = df_features.values
y = df_target.loc[:,"Default"].values
X_train, X_test, y_train, y_test = train_test_split(X, y)

##============================Random Forest Classifier=====================================


seed = 123
 
# RFC with fixed hyperparameters max_depth, max_features and min_samples_leaf
clf_RF = RandomForestClassifier(n_jobs=-1, oob_score = True, n_estimators=128, max_depth=10, max_features='sqrt', min_samples_leaf = 1, min_samples_split=3, random_state=0)
# Range of `n_estimators` values to explore.
n_estim =  list(range(10,14))
 
cv_scores = []
 
for i in n_estim:
    clf_RF .set_params(n_estimators=i)
    kfold = model_selection.KFold(n_splits=5, random_state=seed)
    scores = model_selection.cross_val_score(clf_RF, X_train, y_train, cv=kfold, scoring='accuracy')
    cv_scores.append(scores.mean()*100)
    
Near_optimal_n_estim = n_estim[cv_scores.index(max(cv_scores))]
print ("The optimal number of estimators is %d with %0.1f%%" % (Near_optimal_n_estim, cv_scores[cv_scores.index(max(cv_scores))]))

plt.plot(n_estim, cv_scores)
plt.xlabel('Number of Estimators')
plt.ylabel('Train Accuracy')
plt.show()
