#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 16 14:47:40 2018

@author: mp
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from pandas import read_csv
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import f1_score
from sklearn.metrics import roc_auc_score
from sklearn.decomposition import PCA

import numpy as np
import matplotlib.pyplot as plt

from scipy.ndimage import convolve
from sklearn import linear_model, datasets, metrics
from sklearn.model_selection import train_test_split
from sklearn.neural_network import BernoulliRBM
from sklearn.pipeline import Pipeline
from sklearn.externals import joblib


df_features = pd.read_csv('dataset/features_train.csv').fillna(0)
df_features.head()
df_target = pd.read_csv("dataset/performance_train.csv")
df_target.head()
X = df_features.values
X=X[:,1:28]
X = (X - np.min(X, 0)) / (np.max(X, 0) + 0.0001)  # 0-1 scaling
#
pca = PCA(n_components=20)
fit = pca.fit(X)
X = pca.transform(X)


Y = df_target.loc[:,"Default"].values
##=================================Logistic regression==================================================

X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=0)
  
LC = linear_model.LogisticRegression(C=5000.0, tol=0.0000001, solver= 'sag',max_iter=300)
LC.fit(X_train, Y_train)
predicted_LC = LC.predict(X_test)
predicted_proba_LC = LC.predict_proba(X_test)[:,1]
y_pred=predicted_LC

##============================Scores=======================================================

train_score_LC = LC.score(X_train, Y_train)
test_score_LC = LC.score(X_test, Y_test)
print("Train score RandomForest :", train_score_LC)
print("Test score RandomForest:", test_score_LC)

###########################################################################################


MA=f1_score(Y_test, y_pred, average='macro')  
MI=f1_score(Y_test, y_pred, average='micro') 
WEI=f1_score(Y_test, y_pred, average='weighted') 

fpr, tpr, thresholds = metrics.roc_curve(np.transpose(Y_test), np.transpose(predicted_proba_LC))


print("The Area Under an ROC Curve :", roc_auc_score(Y_test,predicted_proba_LC))

lw=2
df = pd.DataFrame(dict(fpr=fpr, tpr=tpr))

plt.figure()
plt.plot(fpr, tpr, color='darkorange')
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('M')
plt.legend(loc="lower right")
plt.show()


#save model
filepath = "classifiers/LC.pkl"
joblib.dump(LC, filepath)

#generate submit file
df_features_sub = pd.read_csv('dataset/features_test.csv')
X_sub = df_features_sub.values
X_sub = X_sub[:,1:]
X_sub = (X_sub - np.min(X_sub, 0)) / (np.max(X_sub, 0) + 0.0001)  # 0-1 scaling
X_sub = pca.transform(X_sub)
y_sub = LC.predict(X_sub)
df_solution = pd.DataFrame({"ID_CPTE":df_features_sub.ID_CPTE,
                            "Default":y_sub})
df_solution.to_csv("solutions/LC.csv", columns=["ID_CPTE","Default"], index=False)


