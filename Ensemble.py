
"""
Created on Fri Jul 13 21:00:47 2018

@author: mp
"""
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
from pandas import read_csv
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import GaussianNB
import matplotlib.pyplot as plt
from itertools import cycle
from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from scipy import interp
from sklearn.metrics import f1_score
import numpy as np
from sklearn import metrics
from sklearn.metrics import roc_auc_score
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.ensemble import VotingClassifier
from sklearn.svm import SVC  
from sklearn import decomposition
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib


###============================Load Data===================================================

df_features = pd.read_csv('dataset/features_train.csv').fillna(0)
df_features.head()
df_target = pd.read_csv("dataset/performance_train.csv")
df_target.head()
X = df_features.values
X=X[:,1:28]
 
#pca = decomposition.PCA(n_components=24)
#fit = pca.fit(X)
#X = pca.transform(X)

Y = df_target.loc[:,"Default"].values
X_train, X_test, y_train, y_test = train_test_split(X, Y)
###============================Ensemble classifier===================================================
seed = 8
n_kfold=5
kfold = model_selection.KFold(n_splits=n_kfold, random_state=seed)
# create different classifiers
estimators = []
model1 = LogisticRegression(random_state=1)
estimators.append(('logistic', model1))
model2= RandomForestClassifier(n_estimators=173, max_depth=None,max_features='sqrt', min_samples_split=5, random_state=1)
estimators.append(('rf', model2))
model3 = DecisionTreeClassifier()
estimators.append(('cart', model3))
model4 = SVC(kernel='rbf', probability=True)
estimators.append(('svm', model4))
model5 = GaussianNB()
estimators.append(('gnb', model5))
model6 = KNeighborsClassifier(n_neighbors=73)
estimators.append(('knn', model6))

# examine the ensemble model
ensemble = VotingClassifier(estimators)
results = model_selection.cross_val_score(ensemble, X, Y, cv=kfold)
cross_val_score=sum(results)/n_kfold
print(cross_val_score)

#save model
ensemble.fit(X,Y)
y_predict = ensemble.predict(X)
score = (y_predict == Y).sum()/len(y_predict)
print("Final score:", score)
joblib.dump(ensemble, "classifiers/ensemble.pkl")

