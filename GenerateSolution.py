import pandas as pd
from sklearn.externals import joblib
from keras.models import load_model

"""
Once you have saved a good model using training_sklearn.py or taining_keras.py,
 run this one to create a submittable file. The filename will be based the 
 classifier that you load. If you submit your file, I suggest renaming the csv 
 afterwards with the score.
Ex: submitted KNN_clf.csv
Score: 0.508605718936288
renamed KNN_clf_0.508.csv
"""

df_features = pd.read_csv('dataset/features_test.csv')
X = df_features.values
X = X[:,1:]

#load data
filename = input("Name of the classifier to LOAD (no default):"
                 "\n(note: specify the extension (.pkl or .h5)"
                 "\n(note: folder is added automatically)")

filepath = "classifiers/" + filename
try:
    if filepath[-4:] == ".pkl":
        clf = joblib.load(filepath)
        filename = filename[:-4]
        y_predict = clf.predict(X)
    elif filepath[-3:] == ".h5":
        clf = load_model(filepath)
        filename = filename[:-3]
        y_predict = clf.predict_classes(X)
        y_predict = y_predict.flatten()
    else:
        print("Make sure your filename ends with '.pkl' or '.h5'")
except:
    print("Could not load", filepath)
    exit(-1)

print("Successfully loaded:", filepath)

df_solution = pd.DataFrame({"ID_CPTE":df_features.ID_CPTE,
                            "Default":y_predict})

#save solution
filepath = "solutions/" + filename + ".csv"
df_solution.to_csv(filepath, columns=["ID_CPTE","Default"], index=False)

print("Successfully saved:", filepath)
