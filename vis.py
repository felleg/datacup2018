
#Created on Wed Jul 11 14:51:31 2018
#Parsa


import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
plt.style.use('classic')
#matplotlib inline
import numpy as np
import pandas as pd
import seaborn as sns


import matplotlib.pyplot as plt
# step 1: Sactter Plot
data_paiements_train = pd.read_csv('dataset/paiements_train.csv')
data_paiements_train.head()
data_paiements_train["ID_CPTE"].value_counts()
data_paiements_train.plot(kind="scatter", x="TRANSACTION_AMT", y="ID_CPTE")

data_facturation_train= pd.read_csv('dataset/facturation_train.csv')
data_facturation_train.head()
data_facturation_train.plot(kind="scatter", x="CurrentTotalBalance", y="ID_CPTE")
data_facturation_train.plot(kind="scatter", x="CashBalance", y="ID_CPTE")
data_facturation_train.plot(kind="scatter", x="CreditLimit", y="ID_CPTE")

data_transactions_train= pd.read_csv('dataset/transactions_train.csv')
data_transactions_train.head()
data_transactions_train.plot(kind="scatter", x="PRIOR_CREDIT_LIMIT_AMT", y="ID_CPTE")
data_transactions_train.plot(kind="scatter", x="TRANSACTION_AMT", y="ID_CPTE")

data_performance_train= pd.read_csv('dataset/performance_train.csv')
data_performance_train.head()
# step 2: Plotting Distributions
sns.set(color_codes=True)
sns.distplot(data_performance_train['Default'].value_counts());

plt.show()

